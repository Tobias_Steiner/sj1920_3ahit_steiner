﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace Anmeldeformular
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void TextBox_TextChanged_1(object sender, TextChangedEventArgs e)
        {

        }

        private void TextBox_TextChanged_2(object sender, TextChangedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

            if (FirstName_Box.Text == "" || LastName_Box.Text == "" || BirthDate_Box.Text == "" || Father_Box.Text == "" ||
                Mother_Box.Text == "" || PLZ_Box.Text == "" || Street_Box.Text == "" || Number_Box.Text == "" || School_Box.Text == "" ||
                Math_Box.Text == "" || German_Box.Text == "" || English_Box.Text == "" || Science_Box.Text == "" ||
                Wunschabteilung_Box.Text == "" || AlternativAbteilung_Box.Text == "")
            {

            }
            else
            {
                StreamWriter sw = new StreamWriter("data.csv");
                sw.WriteLine(FirstName_Box.Text);
                sw.WriteLine(LastName_Box.Text);
                sw.WriteLine(BirthDate_Box.Text);
                sw.WriteLine(Father_Box.Text);
                sw.WriteLine(Mother_Box.Text);
                sw.WriteLine(PLZ_Box.Text);
                sw.WriteLine(Street_Box.Text);
                sw.WriteLine(Number_Box.Text);
                sw.WriteLine(School_Box.Text);
                sw.WriteLine(Math_Box.Text);
                sw.WriteLine(German_Box.Text);
                sw.WriteLine(English_Box.Text);
                sw.WriteLine(Science_Box.Text);
                sw.WriteLine(Wunschabteilung_Box.Text);
                sw.WriteLine(AlternativAbteilung_Box.Text);
                sw.Close();

            }
        }
    }
}
